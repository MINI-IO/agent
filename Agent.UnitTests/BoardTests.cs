﻿using Agent.Model;
using System;
using Xunit;

namespace Agent.UnitTests
{
    public class BoardTests
    {
        private readonly IBoard board;
        private readonly int width;
        private readonly int height;
        private readonly int goalHeight;
        private readonly Point playerPosition;
        private readonly Team team;


        public BoardTests()
        {
            width = 5;
            height = 10;
            goalHeight = 2;
            playerPosition = new Point(0, 0);
            team = Team.Blue;
            board = new Board(width, height, goalHeight, playerPosition, team);
        }

        [Fact]
        public void ctor__CorrectAssignment()
        {
            Assert.Equal(width, board.Width);
            Assert.Equal(height, board.Height);
        }

        [Fact]
        public void ctor__CorrectBoard()
        {
            for (int x = 0; x < width; ++x)
            {
                for (int y = 0; y < height; ++y)
                {
                    if (y < goalHeight || y >= height - goalHeight)
                    {
                        Assert.Equal(FieldInfo.GoalArea, board.GetField(new Point(x, y)).FieldInfo);
                    }
                    else
                    {
                        Assert.Equal(FieldInfo.TaskArea, board.GetField(new Point(x, y)).FieldInfo);
                    }
                }
            }
        }

        [Theory]
        [InlineData(-1, -1)]
        [InlineData(-1, 1)]
        [InlineData(1, -1)]
        [InlineData(1, 10)]
        [InlineData(5, 1)]
        public void GetField_WrongCoordinates_ThrowsException(int x, int y)
        {
            Point point = new Point(x, y);
            Assert.Throws<ArgumentOutOfRangeException>(() => board.GetField(point));
        }

        [Fact]
        public void GetField_CorrectData_ReturnsCorrectField()
        {
            Field expectedField = new Field()
            {
                DistToPiece = -1,
                FieldInfo = FieldInfo.GoalArea
            };
            Field boardField = board.GetField(new Point(0, 0));

            Assert.Equal(expectedField.DistToPiece, boardField.DistToPiece);
            Assert.Equal(expectedField.FieldInfo, boardField.FieldInfo);
            Assert.Equal(expectedField.PlayerInfo, boardField.PlayerInfo);
        }

        [Theory]
        [InlineData(Direction.negativeX, 0, 0)]
        [InlineData(Direction.negativeY, 0, 0)]
        [InlineData(Direction.positiveX, 4, 7)]
        [InlineData(Direction.positiveY, 4, 7)] //to opponent's goal area
        public void CanMove_WrongDirection_ReturnsFalse(Direction direction, int x, int y)
        {
            Point point = new Point(x, y);
            Board testBoard = new Board(width, height, goalHeight, point, team);
            Assert.False(testBoard.CanMove(direction));
        }

        [Theory]
        [InlineData(Direction.negativeX, 5, 0)]
        [InlineData(Direction.positiveY, -1, 0)]
        [InlineData(Direction.positiveX, 0, 10)]
        [InlineData(Direction.positiveY, 0, -1)]
        [InlineData(Direction.positiveY, 0, 8)] //opponent's goal area
        public void CanMove_WrongCoordinates_ReturnsFalse(Direction direction, int x, int y)
        {
            Point point = new Point(x, y);
            Board testBoard = new Board(width, height, goalHeight, point, team);
            Assert.False(testBoard.CanMove(direction));
        }

        [Theory]
        [InlineData(Direction.positiveX, 0, 0)]
        [InlineData(Direction.positiveY, 0, 0)]
        [InlineData(Direction.negativeX, 4, 7)]
        [InlineData(Direction.negativeY, 4, 7)]
        public void CanMove_CorrectArguments_ReturnsTrue(Direction direction, int x, int y)
        {
            Point point = new Point(x, y);
            Board testBoard = new Board(width, height, goalHeight, point, team);
            Assert.True(testBoard.CanMove(direction));
        }

        [Theory]
        [InlineData(Direction.negativeX, 5, 0)]
        [InlineData(Direction.positiveY, -1, 0)]
        [InlineData(Direction.positiveX, 0, 10)]
        [InlineData(Direction.positiveY, 0, -1)]
        [InlineData(Direction.positiveY, 0, 8)] //opponent's goal area
        public void Move_WrongDirection_ThrowsException(Direction direction, int x, int y)
        {
            Point point = new Point(x, y);
            Board testBoard = new Board(width, height, goalHeight, point, team);
            Assert.Throws<InvalidOperationException>(() => testBoard.Move(direction));
        }

        [Theory]
        [InlineData(Direction.positiveX, 0, 0)]
        [InlineData(Direction.positiveY, 0, 0)]
        [InlineData(Direction.negativeX, 4, 7)]
        [InlineData(Direction.negativeY, 4, 7)]
        public void Move_CorrectArguments_ChangedPosition(Direction direction, int x, int y)
        {
            Point point = new Point(x, y);
            Board testBoard = new Board(width, height, goalHeight, point, team);
            Point newPosition;
            switch (direction)
            {
                case Direction.positiveY:
                    newPosition = new Point(x, y + 1);
                    break;
                case Direction.negativeY:
                    newPosition = new Point(x, y - 1);
                    break;
                case Direction.positiveX:
                    newPosition = new Point(x + 1, y);
                    break;
                case Direction.negativeX:
                    newPosition = new Point(x - 1, y);
                    break;
                default:
                    newPosition = new Point(x, y);
                    break;
            }
            testBoard.Move(direction);
            Assert.Equal(newPosition, testBoard.PlayerPosition);
        }

        [Theory]
        [InlineData(PutPieceInfo.Goal, -1, -1)]
        [InlineData(PutPieceInfo.Goal, 10, 10)]
        [InlineData(PutPieceInfo.Sham, 4, 9)]
        [InlineData(PutPieceInfo.Task, 4, 4)] //task area
        public void PutPiece_WrongArea_ThrowsException(PutPieceInfo piece, int x, int y)
        {
            Point point = new Point(x, y);
            Board testBoard = new Board(width, height, goalHeight, point, team);
            Assert.Throws<InvalidOperationException>(() => testBoard.PutPiece(piece));
        }

        [Fact]
        public void PutPiece_ShamPiece_DoesNothing()
        {
            board.PutPiece(PutPieceInfo.Sham);
            Assert.Equal(playerPosition, board.PlayerPosition);
        }

        [Fact]
        public void PutPiece_Goal_ChangeFieldInfo()
        {
            board.PutPiece(PutPieceInfo.Goal);
            Assert.Equal(FieldInfo.DiscoveredGoal, board.GetField(board.PlayerPosition).FieldInfo);
        }

        [Fact]
        public void PutPiece_NonGoal_MarksNonGoal()
        {
            board.PutPiece(PutPieceInfo.NonGoal);
            Assert.Equal(FieldInfo.DiscoveredNonGoal, board.GetField(board.PlayerPosition).FieldInfo);
        }
    }


}
