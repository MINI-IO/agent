using Agent.Model;
using Agent.Model.Strategies;
using NSubstitute;
using System;
using System.Collections.Generic;
using Xunit;

namespace Agent.UnitTests
{
    public class PlayerTests
    {
        private readonly int id;
        private readonly StrategyBase strategy;
        private readonly Team team;
        private readonly PenaltyTimes penaltyTimes;
        private readonly int leaderId;
        private readonly IBoard board;
        private readonly SortedSet<int> teamMates;
        private readonly int otherPlayerId1;
        private readonly int otherPlayerId2;

        private Player samplePlayer;

        public PlayerTests()
        {
            id = 1;
            strategy = Substitute.For<StrategyBase>(
                Substitute.For<ICommandExecuter>());
            team = Team.Blue;
            penaltyTimes = new PenaltyTimes(500, 500, 500, 500, 500, 500);
            leaderId = 2;
            board = Substitute.For<IBoard>();
            board.CanMove(default).ReturnsForAnyArgs(true);
            otherPlayerId1 = 3;
            otherPlayerId2 = 4;
            teamMates = new SortedSet<int> { leaderId, otherPlayerId1, otherPlayerId2 };

            samplePlayer = new Player(
                id,
                strategy,
                team,
                penaltyTimes,
                leaderId,
                board,
                teamMates);
        }

        [Fact]
        public void ctor__LeavesWaitingPLayersEmpty()
        {
            Assert.Empty(samplePlayer.PlayersWaitingForInfo);
        }

        [Fact]
        public void IsLeader_WhenPlayerIsNotLeader_IsFalse()
        {
            Assert.False(samplePlayer.IsLeader);
        }

        [Fact]
        public void IsLeader_WhenPlayerIsLeader_IsTrue()
        {
            samplePlayer = new Player(
                id,
                strategy,
                team,
                penaltyTimes,
                id,
                board,
                teamMates);

            Assert.True(samplePlayer.IsLeader);
        }

        //[Theory]
        //[InlineData(Direction.negativeX)]
        //[InlineData(Direction.negativeY)]
        //[InlineData(Direction.positiveX)]
        //[InlineData(Direction.positiveY)]
        //public void Move_WhenCanMove_MovesOnTheBoard(Direction direction)
        //{
        //    samplePlayer.Move(direction, 2);

        //    board.Received().Move(direction);
        //    board.Received().UpdateDistanceToPiece(new PieceDistUpdate(PieceDirection.CURRENT, 2));
        //}

        //[Fact]
        //public void Move_WhenCantMove_DoesNotMove()
        //{
        //    board.CanMove(default).ReturnsForAnyArgs(false);
        //    samplePlayer.Move(default, 1);

        //    board.DidNotReceiveWithAnyArgs().Move(default);
        //}

        [Fact]
        public void RequestsResponseBy__AddsIdToTheList()
        {
            samplePlayer.RequestResponseBy(otherPlayerId1);

            Assert.Contains(otherPlayerId1, samplePlayer.PlayersWaitingForInfo);
        }

        [Fact]
        public void GiveInfo_WhenIdIsInTheList_RemovesIdFromTheList()
        {
            samplePlayer.PlayersWaitingForInfo.Add(otherPlayerId1);

            samplePlayer.GiveInfo(otherPlayerId1);

            Assert.DoesNotContain(otherPlayerId1, samplePlayer.PlayersWaitingForInfo);
        }

        [Fact]
        public void GiveInfo_WhenIdIsNotInTheList_Throws()
        {
            Assert.Throws<InvalidOperationException>(() => samplePlayer.GiveInfo(otherPlayerId1));
        }

        [Fact]
        public void Put_WhenPlayerDoesNotHavePiece_Throws()
        {
            Assert.Throws<InvalidOperationException>(() => samplePlayer.Put(default));
        }

        [Theory]
        [InlineData(Piece.Unverified)]
        [InlineData(Piece.Verified)]
        public void Put_WhenPlayerDoesHavePiece_DropsPlayersPiece(Piece piece)
        {
            samplePlayer.Piece = piece;
            PutPieceInfo pieceInfo = PutPieceInfo.Goal;
            samplePlayer.Put(pieceInfo);
            samplePlayer.Board.Received().PutPiece(pieceInfo);
            Assert.Equal(Piece.None, samplePlayer.Piece);
        }

        [Fact]
        public void AcceptMessage__InformsTheBoard()
        {
            samplePlayer.AcceptMessage(/*TODO*/);
            board.Received().AcquireInfo(/*TODO*/);
        }

        [Fact]
        public void Move__IsPenaltied()
        {
            samplePlayer.Move(default, 1);

            Assert.True(samplePlayer.PenaltyTimer.Enabled);
        }

        [Fact]
        public void Put__IsPenaltied()
        {
            samplePlayer.Piece = Piece.Unverified;
            samplePlayer.Put(PutPieceInfo.NonGoal);

            Assert.True(samplePlayer.PenaltyTimer.Enabled);
        }

        [Fact]
        public void AtLeastOneInformationMethod__IsPenaltied()
        {
            samplePlayer.AcceptMessage(/*TODO*/);
            samplePlayer.RequestResponseBy(default);
            samplePlayer.GiveInfo(default);
            samplePlayer.BegForInfo();

            Assert.True(samplePlayer.PenaltyTimer.Enabled);
        }
    }
}