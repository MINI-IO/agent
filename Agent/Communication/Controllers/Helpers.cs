﻿using Agent.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace Agent.Communication
{
    public static class Helpers
    {
        public static Team stringToTeam(string str)
            => str switch
            {
                "red" => Team.Red,
                "blue" => Team.Blue,
                _ => throw new NotImplementedException(),
            };

        public static PutPieceInfo stringToPutPieceInfo(string str)
            => str switch
            {
                "NormalOnGoalField" => PutPieceInfo.Goal,
                "NormalOnNonGoalField" => PutPieceInfo.NonGoal,
                "TaskField" => PutPieceInfo.Task,
                "ShamOnGoalArea" => PutPieceInfo.Sham,
                _ => throw new NotImplementedException(),
            };
    }
}
