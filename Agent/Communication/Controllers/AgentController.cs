﻿using Agent.Communication.DTOs;
using Agent.Communication.DTOs.Receive;
using Agent.Model;
using Agent.Model.Strategies;
using IoCommunication;
using System;
using System.Threading;
using System.Collections.Generic;

namespace Agent.Communication
{
    public class AgentController : IController
    {
        private readonly ISingleAgentRepository _repository;

        public ICommandExecuter _commandExecuter { get; set; } = null!;

        private Player Player => _repository.Single();

        CancellationTokenSource CancellationTokenSource;

        public AgentController(
            ISingleAgentRepository repository,
            CancellationTokenSource cancellation
            )
        {
            this._repository = repository;
            this.CancellationTokenSource = cancellation;
        }

        [IoConnMethod(101)]
        public void ShamCheck(MessageWrapperDTO mw, CheckShamDTO checkSham)
        {
            Player.ShamCheck(checkSham.Sham);
        }

        [IoConnMethod(102)]
        public void DestroyPiece(MessageWrapperDTO mw, DestroyPieceDTO destroyPiece)
        {
            Player.DestroyPiece();
        }

        [IoConnMethod(103)]
        public void Discovery(MessageWrapperDTO mw, DiscoveryDTO discovery)
        {
            List<PieceDistUpdate> updates = new List<PieceDistUpdate>();
            updates.Add(new PieceDistUpdate(PieceDirection.CURRENT,
                discovery.DistanceFromCurrent));
            updates.Add(new PieceDistUpdate(PieceDirection.N,
                discovery.DistanceN));
            updates.Add(new PieceDistUpdate(PieceDirection.S,
                discovery.DistanceS));
            updates.Add(new PieceDistUpdate(PieceDirection.E,
                discovery.DistanceE));
            updates.Add(new PieceDistUpdate(PieceDirection.W,
                discovery.DistanceW));
            updates.Add(new PieceDistUpdate(PieceDirection.NE,
                discovery.DistanceNE));
            updates.Add(new PieceDistUpdate(PieceDirection.NW,
                discovery.DistanceNW));
            updates.Add(new PieceDistUpdate(PieceDirection.SE,
                discovery.DistanceSE));
            updates.Add(new PieceDistUpdate(PieceDirection.SW,
                discovery.DistanceSW));
            Player.Discovery(updates);
        }

        [IoConnMethod(104)]
        public void GameEnd(MessageWrapperDTO mw, GameEndDTO gameEndss)
        {
            Console.WriteLine("GAME ENDED");
            CancellationTokenSource.Cancel();
        }

        [IoConnMethod(105)]
        public void StartGame(MessageWrapperDTO mw, GameStartDTO gameStart)
        {
            Point playerPosition = new Point(
                gameStart.Position.X,
                gameStart.Position.Y
                );
            Team team = Helpers.stringToTeam(gameStart.TeamID);
            Board board = new Board(
                gameStart.BoardSize.X,
                gameStart.BoardSize.Y,
                gameStart.GoalAreaSize,
                playerPosition,
                team
                );
            PenaltyTimes penaltyTimes = new PenaltyTimes(
                int.Parse(gameStart.Penalties.Move) + 300,
                int.Parse(gameStart.Penalties.CheckForScham),
                int.Parse(gameStart.Penalties.Discovery),
                int.Parse(gameStart.Penalties.DestroyPiece),
                int.Parse(gameStart.Penalties.PutPiece),
                int.Parse(gameStart.Penalties.InformationExchange)
                );

            var player = new Player(
                gameStart.AgentID,
                new BasicStrategy(_commandExecuter),
                team,
                penaltyTimes,
                gameStart.LeaderID,
                board,
                gameStart.AlliesIDs
                );

            _repository.Register(player);
        }

        [IoConnMethod(106)]
        public void SomebodyAsked(MessageWrapperDTO mw, dynamic d)
        {
            Player.RequestResponseBy(d);
        }

        [IoConnMethod(107)]
        public void Join(MessageWrapperDTO mw, JoinResponseDTO joinResponse)
        {
            if(!joinResponse.Accepted)
            {
                Console.WriteLine("AGENT NOT ACCEPTED");
                CancellationTokenSource.Cancel();
            }
        }

        [IoConnMethod(108)]
        public void MoveAgent(MessageWrapperDTO mw, MoveDTO moveResponse)
        {
            if(!moveResponse.MadeMove)
            {
                return;
            }
            
            if(moveResponse.PickedUpPiece)
            {
                Player.Piece = Piece.Unverified;
            }

            var p = new Point(moveResponse.CurrentPosition.X, moveResponse.CurrentPosition.Y);
            Player.Move(p, moveResponse.ClosestPiece);
        }

        [IoConnMethod(109)]
        public void PickUpPiece(MessageWrapperDTO mw, PickUpPieceDTO pickUpPiece)
        {
            Player.Piece = Piece.Unverified;
        }

        [IoConnMethod(110)]
        public void PutPiece(MessageWrapperDTO mw, PutPieceDTO putPiece)
        {
            Player.Put(
                Helpers.stringToPutPieceInfo(putPiece.PutPieceInfo));
        }

        [IoConnMethod(111)]
        public void Pause(MessageWrapperDTO mw, dynamic pause)
        {
            Player.PenaltyTimer.Stop();
        }

        [IoConnMethod(112)]
        public void Resume(MessageWrapperDTO mw, dynamic resume)
        {
            Player.PenaltyTimer.Start();
        }
    }
}
