﻿namespace Agent.Communication.DTOs.Requests
{
    public class MoveDTO
    {
        public string Direction { get; set; } = null!;
    }
}
