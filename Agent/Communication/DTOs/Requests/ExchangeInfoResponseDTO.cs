﻿using System.Collections.Generic;

namespace Agent.Communication.DTOs.Requests
{
    public class ExchangeInfoResponseDTO
    {
        public int RespondToID { get; set; }
        public List<double> Distances { get; set; } = new List<double>();
        public List<string> RedTeamGoalAreaInformations { get; set; } = new List<string>();
        public List<string> BlueTeamGoalAreaInformations { get; set; } = new List<string>();
    }
}
