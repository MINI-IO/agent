﻿namespace Agent.Communication.DTOs
{
    public class MessageWrapperDTO
    {
        public int MessageID { get; set; }
        public int? AgentID { get; set; } = null!;
        public object Payload { get; set; } = null!;
    }
}
