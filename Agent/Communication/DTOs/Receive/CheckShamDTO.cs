﻿namespace Agent.Communication.DTOs.Receive
{
    public class CheckShamDTO
    {
        public bool Sham { get; set; }
    }
}
