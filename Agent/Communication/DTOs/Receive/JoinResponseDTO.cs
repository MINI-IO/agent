﻿namespace Agent.Communication.DTOs.Receive
{
    public class JoinResponseDTO
    {
        public bool Accepted { get; set; }
        public int AgentId { get; set; }
    }
}
