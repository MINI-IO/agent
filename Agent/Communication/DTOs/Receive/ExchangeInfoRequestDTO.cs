﻿namespace Agent.Communication.DTOs.Receive
{
    public class ExchangeInfoRequestDTO
    {
        public int AskingID { get; set; }
        public bool Leader { get; set; }
        public string TeamId { get; set; } = string.Empty;
    }
}
