﻿namespace Agent.Communication.DTOs.Receive
{
    public class GameEndDTO
    {
        public string Winner { get; set; } = string.Empty;
    }
}
