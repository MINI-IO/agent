﻿using System;

namespace Agent.Communication.DTOs.Receive
{
    public class PenaltyErrorDTO
    {
        public DateTime WaitUntil { get; set; }
    }
}
