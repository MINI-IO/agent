﻿namespace Agent.Communication.DTOs.Receive
{
    public class UndefinedErrorDTO
    {
        public PositionDTO Position { get; set; } = null!;
        public bool HoldingPiece { get; set; }

        public class PositionDTO
        {
            public int x { get; set; }
            public int y { get; set; }
        }
    }
}
