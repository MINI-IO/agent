﻿namespace Agent.Communication.DTOs.Receive
{
    internal class WrongMoveDTO
    {
        public PositionDTO Position { get; set; } = null!;

        public class PositionDTO
        {
            public int X { get; set; }
            public int Y { get; set; }
        }
    }
}
