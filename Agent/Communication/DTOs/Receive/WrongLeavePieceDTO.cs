﻿namespace Agent.Communication.DTOs.Receive
{
    public class WrongLeavePieceDTO
    {
        public string ErrorSubtype { get; set; } = string.Empty;
    }
}
