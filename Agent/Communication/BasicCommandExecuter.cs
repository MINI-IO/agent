﻿using Agent.Model;
using IoCommunication;
using System;
using Agent.Communication.DTOs.Requests;
using Agent.Communication.DTOs;

namespace Agent.Communication
{
    internal class BasicCommandExecuter : ICommandExecuter
    {
        private readonly IMessageSender _messageSender;

        public BasicCommandExecuter(IMessageSender messageSender)
        {
            _messageSender = messageSender;
        }

        public void RequestInformationExchange(int askedAgentID) { }
        public void AnswerInformationExchange(int respondToID, IBoard board) 
        {
            var message = new MessageWrapperDTO()
            {
                MessageID = 004,
                Payload = new ExchangeInfoResponseDTO
                {
                    RespondToID = respondToID,
                },
            };

            _messageSender.SendMessage(message);
        }

        public void DestroyPiece()
        {
            var message = new MessageWrapperDTO()
            {
                MessageID = 002,
            };

            _messageSender.SendMessage(message);
        }

        public void Discover()
        {
            var message = new MessageWrapperDTO()
            {
                MessageID = 003,
            };

            _messageSender.SendMessage(message);
        }

        public void IsSham()
        {
            var message = new MessageWrapperDTO()
            {
                MessageID = 001,
            };

            _messageSender.SendMessage(message);
        }

        public void JoinTheGame(Team team)
        {
            var message = new MessageWrapperDTO()
            {
                MessageID = 006,
                Payload = new JoinTheGameDTO
                {
                    TeamId = team == Team.Red ? "red" : "blue",
                }
            };

            _messageSender.SendMessage(message);
        }

        public void Move(Direction direction)
        {
            string dir = direction switch
            {
                Direction.negativeX => "negativeX",
                Direction.positiveX => "positiveX",
                Direction.negativeY => "negativeY",
                Direction.positiveY => "positiveY",
                _ => throw new NotImplementedException(),
            };

            var message = new MessageWrapperDTO()
            {
                MessageID = 007,
                Payload = new MoveDTO
                {
                    Direction = dir,
                },
            };

            _messageSender.SendMessage(message);
        }

        public void PickUpPiece()
        {
            var message = new MessageWrapperDTO()
            {
                MessageID = 008,
            };

            _messageSender.SendMessage(message);
        }

        public void PutPiece()
        {
            var message = new MessageWrapperDTO()
            {
                MessageID = 009,
            };

            _messageSender.SendMessage(message);
        }

    }
}
