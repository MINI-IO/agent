﻿using Agent.Model.Strategies;
using System;
using System.Collections.Generic;
using System.Timers;

namespace Agent.Model
{
    public enum Team
    {
        Red,
        Blue
    }

    public enum Piece
    {
        None,
        Verified,
        Unverified,
        Sham
    }

    public enum PutPieceInfo
    {
        Sham,
        Goal,
        NonGoal,
        Task
    }

    public struct PenaltyTimes
    {
        public int Move { get; }
        public int CheckForSham { get; }
        public int Discovery { get; }
        public int DestroyPiece { get; }
        public int PutPiece { get; }
        public int InformationExchange { get; }

        public PenaltyTimes(
            int move,
            int checkForSham,
            int discovery,
            int destroyPiece,
            int putPiece,
            int informationExchange)
        {
            Move = move;
            CheckForSham = checkForSham;
            Discovery = discovery;
            DestroyPiece = destroyPiece;
            PutPiece = putPiece;
            InformationExchange = informationExchange;
        }
    }


    public class Player
    {
        private readonly int Id;
        private readonly IStrategy Strategy;
        private readonly PenaltyTimes PenaltyTimes;
        private const int strategyTimeout = 1000;

        public Timer PenaltyTimer { get; private set; } = new Timer();
        public Team Team { get; private set; }
        public int LeaderId { get; private set; }
        public bool IsLeader => Id == LeaderId;
        public Piece Piece { get; set; }
        public IBoard Board { get; set; }
        public List<int> PlayersWaitingForInfo { get; } = new List<int>();
        public SortedSet<int> TeamMates { get; private set; } = new SortedSet<int>();

        public Player(
            int id,
            IStrategy strategy,
            Team team,
            PenaltyTimes penaltyTimes,
            int leaderId,
            IBoard board,
            IEnumerable<int> teamMates)
        {
            this.Id = id;
            this.Strategy = strategy;
            Team = team;
            this.PenaltyTimes = penaltyTimes;
            LeaderId = leaderId;
            Board = board;
            TeamMates.UnionWith(teamMates);

            PenaltyTimer.Elapsed += MakeDecision;
            PenaltyTimer.Interval = 1000;
            PenaltyTimer.AutoReset = false;
            PenaltyTimer.Start();
        }

        public void Stop()
        {
            throw new NotImplementedException();
        }

        public void Move(Point direction, int? distance)
        {
            Board.Move(direction);
            var update = new PieceDistUpdate(PieceDirection.CURRENT, distance);
            Board.UpdateDistanceToPiece(update);
            Penalty(PenaltyTimes.Move);
        }

        public void Put(PutPieceInfo pieceInfo)
        {
            if (Piece == Piece.None)
            {
                throw new InvalidOperationException();
            }
            Piece = Piece.None;
            Penalty(PenaltyTimes.PutPiece);
            if (pieceInfo != PutPieceInfo.Goal && pieceInfo != PutPieceInfo.NonGoal)
            {
                return;
            }
            Board.PutPiece(pieceInfo);
        }

        public IBoard GiveInfo(int id)
        {
            if (!PlayersWaitingForInfo.Contains(id))
            {
                throw new InvalidOperationException();
            }
            PlayersWaitingForInfo.Remove(id);
            Penalty(PenaltyTimes.InformationExchange);
            return Board;
        }

        public void RequestResponseBy(int id)
        {
            PlayersWaitingForInfo.Add(id);
        }

        public void AcceptMessage(/*TODO*/)
        {
            Board.AcquireInfo(/*TODO*/);
        }

        public void DestroyPiece()
        {
            if(Piece == Piece.None)
            {
                throw new InvalidOperationException();
            }
            Piece = Piece.None;
            Penalty(PenaltyTimes.DestroyPiece);
        }

        public void BegForInfo()
        {
            Penalty(PenaltyTimes.InformationExchange);
        }

        public void ShamCheck(bool sham)
        {
            Penalty(PenaltyTimes.CheckForSham);
            if (!sham)
            {
                Piece = Piece.Verified;
            }
            else
            {
                Piece = Piece.Sham;
            }
        }

        public void MakeDecision(object sender, ElapsedEventArgs e)
        {
            Strategy.MakeDecision(this);
            Penalty(strategyTimeout);
        }
        
        public void Discovery(List<PieceDistUpdate> pieceDists)
        {
            foreach(PieceDistUpdate update in pieceDists)
            {
                Board.UpdateDistanceToPiece(update);
            }
            Penalty(PenaltyTimes.Discovery);
        }

        private void Penalty(int time)
        {
            PenaltyTimer.Stop();
            PenaltyTimer.Interval = time;
            PenaltyTimer.Start();
        }
    }
}
