﻿using Agent.Model;

namespace Agent.Model
{
    public class PlayerInfo
    {
        public int[] Distances { get; private set; }
        public FieldInfo[] FieldInfos { get; private set; }

        public PlayerInfo(int[] distances, FieldInfo[] fieldInfos)
        {
            Distances = distances;
            FieldInfos = fieldInfos;
        }
    }
}