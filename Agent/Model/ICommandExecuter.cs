﻿namespace Agent.Model
{
    public interface ICommandExecuter
    {
        public void IsSham();
        public void DestroyPiece();
        public void Discover();
        public void AnswerInformationExchange(int respondToID, IBoard board);
        public void RequestInformationExchange(int askedAgentID);
        public void JoinTheGame(Team team);
        public void Move(Direction direction);
        public void PickUpPiece();
        public void PutPiece();
    }
}
