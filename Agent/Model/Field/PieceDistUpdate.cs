namespace Agent.Model
{
    public struct PieceDistUpdate
    {
        public PieceDirection direction;
        public int distance;
        public PieceDistUpdate(PieceDirection direction, int? distance)
        {
            this.direction = direction;
            this.distance = distance ??= -1;
        }
    }
    public enum PieceDirection
    {
        N, NE, E, SE, S, SW, W, NW, CURRENT
    }
}
