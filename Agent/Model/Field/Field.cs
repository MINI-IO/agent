﻿namespace Agent.Model
{
    public enum FieldInfo
    {
        TaskArea,
        GoalArea,
        DiscoveredNonGoal,
        DiscoveredGoal
    }

    public class Field
    {
        public FieldInfo FieldInfo { get; set; }
        public bool PlayerInfo { get; set; }
        public int DistToPiece { get; set; }
    }
}
