﻿namespace Agent.Model
{
    public interface IBoard
    {
        public int Width { get; }
        public int Height { get; }
        public Point PlayerPosition { get; }
        public int GoalHeight { get; }

        public bool CanMove(Direction direction);
        public void Move(Direction direction);
        public void Move(Point direction);
        public void PutPiece(PutPieceInfo pieceInfo);
        public Field GetField(Point point);
        public void UpdateDistanceToPiece(PieceDistUpdate update);
        public void AcquireInfo(/*TODO*/);
    }
}
