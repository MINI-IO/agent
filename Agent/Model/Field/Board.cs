﻿using System;

namespace Agent.Model
{
    public class Board : IBoard
    {
        public int Width { get; private set; }

        public int Height { get; private set; }

        public Point PlayerPosition { get; private set; }
        public int GoalHeight { get; private set; }
        private readonly Team team;
        private readonly Field[,] board;
        public Board(int width, int height, int goalHeight,
            Point playerPosition, Team team)
        {
            Width = width;
            Height = height;
            this.GoalHeight = goalHeight;
            PlayerPosition = playerPosition;
            board = new Field[Height, Width];
            this.team = team;
            for (int i = 0; i < Height; i++)
            {
                for (int j = 0; j < Width; j++)
                {
                    board[i, j] = new Field
                    {
                        DistToPiece = -1
                    };
                    if (i < goalHeight || i + goalHeight >= Height)
                    {
                        board[i, j].FieldInfo = FieldInfo.GoalArea;
                    }
                    else
                    {
                        board[i, j].FieldInfo = FieldInfo.TaskArea;
                    }
                }
            }
        }

        public bool CanMove(Direction direction)
        {
            if (PlayerPosition.X < 0 || PlayerPosition.X >= Width)
            {
                return false;
            }
            if (PlayerPosition.Y < 0 || PlayerPosition.Y >= Height)
            {
                return false;
            }

            var positionAfterMove = MovePoint(direction, PlayerPosition);

            bool exceedsBoard = direction switch
            {
                Direction.positiveY => positionAfterMove.Y >= Height,
                Direction.negativeY => positionAfterMove.Y < 0,
                Direction.positiveX => positionAfterMove.X >= Width,
                Direction.negativeX => positionAfterMove.X < 0,
                _ => throw new InvalidOperationException("Invalid direction"),
            };

            bool disallowedField = team switch
            {
                Team.Red => positionAfterMove.Y < GoalHeight,
                Team.Blue => positionAfterMove.Y + GoalHeight >= Height,
                _ => throw new InvalidOperationException("Invalid team"),
            };

            return !exceedsBoard && !disallowedField;
        }

        public Field GetField(Point point)
        {
            if (point.X < 0 || point.X >= Width)
            {
                throw new ArgumentOutOfRangeException();
            }
            if (point.Y < 0 || point.Y >= Height)
            {
                throw new ArgumentOutOfRangeException();
            }
            return board[point.Y, point.X];
        }

        public void Move(Direction direction)
        {
            if (CanMove(direction))
            {
                PlayerPosition = MovePoint(direction, PlayerPosition);
            }
            else
            {
                throw new InvalidOperationException();
            }
        }

        private Point MovePoint(Direction direction, Point point)
        {
            return direction switch
            {
                Direction.positiveY => new Point(point.X, point.Y + 1),
                Direction.negativeY => new Point(point.X, point.Y - 1),
                Direction.positiveX => new Point(point.X + 1, point.Y),
                Direction.negativeX => new Point(point.X - 1, point.Y),
                _ => throw new InvalidOperationException("Invalid direction"),
            };
        }

        private Point MovePoint(PieceDirection direction, Point point)
        {
            return direction switch
            {
                PieceDirection.N => new Point(point.X, point.Y + 1),
                PieceDirection.S => new Point(point.X, point.Y - 1),
                PieceDirection.E => new Point(point.X + 1, point.Y),
                PieceDirection.W => new Point(point.X - 1, point.Y),
                PieceDirection.NE => new Point(point.X + 1, point.Y + 1),
                PieceDirection.NW => new Point(point.X - 1, point.Y + 1),
                PieceDirection.SE => new Point(point.X + 1, point.Y - 1),
                PieceDirection.SW => new Point(point.X - 1, point.Y - 1),
                PieceDirection.CURRENT => point,
                _ => throw new InvalidOperationException("Invalid direction"),
            };
        }

        public void PutPiece(PutPieceInfo pieceInfo)
        {
            if (PlayerPosition.X < 0 || PlayerPosition.X >= Width)
            {
                throw new InvalidOperationException();
            }
            if (PlayerPosition.Y < 0 || PlayerPosition.Y >= Height)
            {
                throw new InvalidOperationException();
            }
            bool inMyGoalArea = team switch
            {
                Team.Blue => PlayerPosition.Y < GoalHeight,
                Team.Red => PlayerPosition.Y + GoalHeight >= Height,
                _ => throw new InvalidOperationException("Invalid team"),
            };
            if (!inMyGoalArea)
            {
                throw new InvalidOperationException();
            }
            switch (pieceInfo)
            {
                case PutPieceInfo.Sham:
                    break;
                case PutPieceInfo.Goal:
                    board[PlayerPosition.Y, PlayerPosition.X].FieldInfo = FieldInfo.DiscoveredGoal;
                    break;
                case PutPieceInfo.NonGoal:
                    board[PlayerPosition.Y, PlayerPosition.X].FieldInfo = FieldInfo.DiscoveredNonGoal;
                    break;
                case PutPieceInfo.Task:
                    break;
                default:
                    break;
            }
        }

        public void AcquireInfo(/*TODO*/)
        {
            throw new NotImplementedException();
        }

        public void UpdateDistanceToPiece(PieceDistUpdate update)
        {
            Field field;
            try
            {
                field = GetField(MovePoint(update.direction,PlayerPosition));
            }catch (ArgumentOutOfRangeException)
            {
                return;
            }
            field.DistToPiece = update.distance;
        }

        public void Move(Point direction)
        {
            PlayerPosition = direction;
        }
    }
}
