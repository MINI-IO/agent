﻿namespace Agent.Model
{
    public enum Direction
    {
        positiveY = 0,
        negativeY = 1,
        positiveX = 2,
        negativeX = 3
    }
}
