﻿namespace Agent.Model.Strategies
{
    public interface IStrategy
    {
        public void MakeDecision(Player player);
    }
}
