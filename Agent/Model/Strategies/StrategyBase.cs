﻿using System;
using System.Linq;

namespace Agent.Model.Strategies
{
    public abstract class StrategyBase : IStrategy
    {
        protected int lastDistanceToPiece = -1;
        protected readonly ICommandExecuter CommandExecuter;
        public StrategyBase(ICommandExecuter commandExecuter)
        {
            this.CommandExecuter = commandExecuter;
        }
        public abstract void MakeDecision(Player player);
        protected Field GetPlayerField(Player player)
            => player.Board.GetField(player.Board.PlayerPosition);
        protected bool HasPieceOnGoalField(Player player)
        {
            return player.Piece != Piece.None &&
            GetPlayerField(player).FieldInfo == FieldInfo.GoalArea;
        }

        protected Point GetTheClosestGoalField(Player player)
        {
            var startHeight = player.Team switch
            {
                Team.Red => player.Board.Height - player.Board.GoalHeight,
                Team.Blue => 0,
                _ => throw new NotImplementedException(
                    $"{player.Team}"),
            };
            var width = Enumerable.Range(0, player.Board.Width);
            var height = Enumerable.Range(startHeight, player.Board.GoalHeight);

            var allCoordinatesEnumerator = from xx in width
                                           from yy in height
                                           select new Point(xx, yy);

            return allCoordinatesEnumerator
                .Where(p => 
                    player.Board.GetField(p).FieldInfo == FieldInfo.GoalArea)
                .OrderBy(p => DistanceToField(player, p))
                .First();
        }
        protected Direction DirectionToField(Player player, Point point)
        {
            if(point.X-player.Board.PlayerPosition.X>0)
            {
                return Direction.positiveX;
            }else if(point.X-player.Board.PlayerPosition.X<0)
            {
                return Direction.negativeX;
            }else if(point.Y-player.Board.PlayerPosition.Y>0)
            {
                return Direction.positiveY;
            }else
            {
                return Direction.negativeY;
            }
        }

        protected int DistanceToField(Player player, Point point)
        {
            //if the field doesn't exist GetField will throw an exception
            _ = player.Board.GetField(point);
            return Math.Abs(point.X - player.Board.PlayerPosition.X) +
                Math.Abs(point.Y - player.Board.PlayerPosition.Y);
        }

        protected bool IsPlayerOnArea(Player player, FieldInfo info)
        {
            var field = player.Board.GetField(player.Board.PlayerPosition);
            return field.FieldInfo == info;
        }

        protected Direction DirectionToTaskArea(Player player)
        {
            return player.Team switch
            {
                Team.Blue => Direction.positiveY,
                Team.Red => Direction.negativeY,
                _ =>throw new NotImplementedException($"{player.Team}"),
            };
        }
    }
}
