﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Agent.Model.Strategies
{
    public class RandomStrategy : StrategyBase
    {
        private readonly Random _random = new Random();

        private int NextActionIndex
            => _random.Next(0, Actions.Count - 1);

        private readonly List<Action> Actions;

        public RandomStrategy(ICommandExecuter commandExecuter)
            : base(commandExecuter)
        {
            Actions = new List<Action>()
            {
                () => CommandExecuter.Move(Direction.negativeX),
                () => CommandExecuter.Move(Direction.negativeY),
                () => CommandExecuter.Move(Direction.positiveX),
                () => CommandExecuter.Move(Direction.positiveY),
                () => CommandExecuter.PutPiece(),
            };
        }

        public override void MakeDecision(Player player)
        {
            Actions[NextActionIndex].Invoke();
        }
    }
}
