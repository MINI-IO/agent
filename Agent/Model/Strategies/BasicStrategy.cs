﻿using System;
using System.Collections.Generic;

namespace Agent.Model.Strategies
{
    public class BasicStrategy : StrategyBase
    {
        public BasicStrategy(ICommandExecuter communicationService)
            : base(communicationService)
        {

        }

        Random random = new Random();
        public override void MakeDecision(Player player)
        {

            if (player.Piece != Piece.None)
            {
                if (player.Board.PlayerPosition.Y >= player.Board.GoalHeight 
                    && player.Board.PlayerPosition.Y < player.Board.Height - player.Board.GoalHeight)
                {
                    if (random.Next(5) != 0)
                    {
                        CommandExecuter.Move(player.Team == Team.Blue ?
                            Direction.negativeY : Direction.positiveY);
                    }
                    else
                    {
                        CommandExecuter.Move(random.Next(0, 2) == 0 ? Direction.negativeX : Direction.positiveX);
                    }
                }
                else
                {
                    if (player.Board.GetField(player.Board.PlayerPosition).FieldInfo == FieldInfo.GoalArea)
                    {
                        CommandExecuter.PutPiece();
                    }
                    else
                    {
                        int dir = random.Next(0, 3);
                        Direction direction = dir switch
                        {
                            0 => Direction.negativeX,
                            1 => Direction.positiveX,
                            2 => player.Team == Team.Blue ?
                            Direction.negativeY : Direction.positiveY,
                        };
                        CommandExecuter.Move(direction);
                    }
                }
            }
            else
            {
                if (player.Board.PlayerPosition.Y >= player.Board.GoalHeight
                    && player.Board.PlayerPosition.Y < player.Board.Height - player.Board.GoalHeight)
                {
                    Direction direction;
                    direction = (Direction)random.Next(0, 4);
                    CommandExecuter.Move(direction);
                }
                else
                {
                    if (random.Next(5) != 0)
                    {
                        CommandExecuter.Move(player.Team == Team.Blue ?
                            Direction.positiveY : Direction.negativeY);
                    }
                    else
                    {
                        CommandExecuter.Move(random.Next(0, 2) == 0 ? Direction.negativeX : Direction.positiveX);
                    }
                }
            }
        }
    }
}
