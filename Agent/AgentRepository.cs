﻿using Agent.Model;
using System;

namespace Agent
{
    public interface ISingleAgentRepository
    {
        public Player Single();
        public void Register(Player player);
    }
    public class SingleAgentRepository : ISingleAgentRepository
    {
        private Player Player = null!;
        public void Register(Player player)
        {
            if (Player is null)
            {
                Player = player;
            }
            else
            {
                throw new InvalidOperationException(
                    "Player already registered");
            }
        }

        public Player Single()
            => Player ?? throw new InvalidOperationException(
                $"Agent not registered");
    }
}
