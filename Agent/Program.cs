﻿using Agent.Communication;
using Agent.Communication.DTOs;
using Agent.Model;
using IoCommunication;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Agent
{
    internal class Program
    {
        public static IHostBuilder CreateHostBuilder<T>(BasicMessageHandler<T> bmh) =>
            Host.CreateDefaultBuilder()
                .ConfigureServices((hostContext, services) =>
                {
                    services.AddSingleton<IMessageHandler>(bmh);
                    services.AddHostedService<TcpClient>();
                });

        private static async Task Main(string[] args)
        {
            List<Task> l = new List<Task>();
            for (int i = 0; i < 10; ++i)
            {
                ISingleAgentRepository repository = new SingleAgentRepository();

                var cancellationTokenSource = new CancellationTokenSource();
                var controller = new AgentController(repository, cancellationTokenSource);

                var messageHandler = new BasicMessageHandler<MessageWrapperDTO>(
                    controller
                    );

                var communicationService = new BasicCommandExecuter(messageHandler);
                controller._commandExecuter = communicationService;

                var host = CreateHostBuilder(messageHandler).Build();

                _ = Task.Run(() => { Thread.Sleep(1000); communicationService.JoinTheGame(i < 5 ? Team.Red : Team.Blue); });
                l.Add(host.RunAsync(cancellationTokenSource.Token));
                Thread.Sleep(2000);
            }
            Task.WaitAll(l.ToArray());
        }
    }
}
