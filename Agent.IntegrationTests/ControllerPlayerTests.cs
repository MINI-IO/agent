using Agent.Communication;
using Agent.Communication.DTOs;
using Agent.Communication.DTOs.Receive;
using Agent.Model;
using NSubstitute;
using NSubstitute.ReceivedExtensions;
using System;
using Xunit;

namespace Agent.IntegrationTests
{
    public class ControllerPlayerTests
    {
        private readonly AgentController controller;
        private readonly Player player;
        private int PlayerId => 1;

        public ControllerPlayerTests()
        {
            player = Substitute.For<Player>(
                PlayerId,
                default,
                Team.Blue,
                new PenaltyTimes(1,1,1,1,1,1),
                PlayerId,
                default,
                new[] { PlayerId });

            var repo = Substitute.For<ISingleAgentRepository>();
            repo.Single().Returns(player);

            controller = new AgentController(repo, default);
        }

        [Fact]
        public void ShamCheck()
        {
            try
            {
                controller.ShamCheck(new MessageWrapperDTO(), new CheckShamDTO());
            }
            catch (Exception) { }

            player.ReceivedWithAnyArgs().ShamCheck(default);
        }

        [Fact]
        public void DestroyPiece()
        {
            try
            {
                controller.DestroyPiece(new MessageWrapperDTO(), new DestroyPieceDTO());

                player.ReceivedWithAnyArgs().DestroyPiece();
            }
            catch (InvalidOperationException) { }
        }

        [Fact]
        public void Discovery()
        {
            try
            {
                controller.Discovery(new MessageWrapperDTO(), new DiscoveryDTO());

                player.ReceivedWithAnyArgs().Discovery(default);
            }
            catch (NullReferenceException) { }
        }

        [Fact]
        public void SomebodyAsked()
        {
            try
            {
                controller.SomebodyAsked(new MessageWrapperDTO(), 3);
            }
            catch (Exception) { }

            player.ReceivedWithAnyArgs().RequestResponseBy(default);
        }

        [Fact]
        public void MoveAgent()
        {
            try
            {
                controller.MoveAgent(new MessageWrapperDTO(), new MoveDTO());

                player.ReceivedWithAnyArgs().Move(default, default);
            }
            catch (NullReferenceException) { }

        }

        [Fact]
        public void PutPiece()
        {
            try
            {
                controller.PutPiece(new MessageWrapperDTO(), new PutPieceDTO() { PutPieceInfo = "NormalOnGoalField" });

                player.ReceivedWithAnyArgs().Put(default);
            }
            catch (InvalidOperationException) { }

        }
    }
}
